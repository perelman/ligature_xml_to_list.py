#!/usr/bin/env python

# This takes in just the ligatures section of a .ttx file:
# USAGE: sed -n '/<LigatureSubst /,/<\/LigatureSubst>/p' FOO.ttx \
#            | ./ligature_xml_to_list.py

from __future__ import print_function

import lxml.etree
import sys

try:
  import fontTools.agl

  def map_glyph(c):
    res = fontTools.agl.AGL2UV.get(c, None)
    if res is not None:
      return unichr(res)
    else:
      return c
except ImportError:
  print('WARNING: Package fonttools not found.', file=sys.stderr)
  # Don't have fonttools package.
  # TODO: 5 ---> five etc.
  glyph_mappings = {
    'space': ' ',
  }

  def map_glyph(c):
    return glyph_mappings.get(c, c)

tree = lxml.etree.parse(sys.stdin)

print('\n'.join(sorted([''.join(map(map_glyph,
                                    [ligSet.attrib['glyph']]
                                      + lig.attrib['components'].split(',')))
                        for ligSet in tree.iterfind('LigatureSet')
                        for lig in ligSet.iterfind('Ligature')
                       ])))
